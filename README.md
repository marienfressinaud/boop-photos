# Boop! Photos — a simple static site generator, for photos.

**Boop! Photos is a static site generator for photos galleries.** I built it
for my personal needs and you should probably not use it, or at your own risks.

**It is designed to be used by Web developers who are comfortable with a
terminal.** Indeed, nothing comes for free in Boop! Photos: you'll have to
create your own templates and your own style, you'll need to setup additional
tooling by yourself, there's nothing like “hot reloading”, etc.

**Boop! Photos is now done. It means no new feature will be added, but it's
still maintained.** I'm not using it a lot these days, and if I had to
implement new features, I would more probably choose another tool. However, I
may encounter some bugs or security issues: I intend to fix them.

## Installation

In this guide, it is assumed that you are using Linux.

First, clone the Boop! Photos repository and install its dependencies. It
requires Python 3.6 to work.

```console
$ git clone https://framagit.org/marien.fressinaud/boop-photos.git
$ cd boop-photos
boop-photos$ pip3 install -r requirements.txt
```

Then, add the path of the repository to your `PATH` environment variable. For
instance, you can add it to your `.bashrc` file:

```console
boop-photos$ # Be sure to be in the boop-photos/ folder
boop-photos$ echo export PATH="$(pwd):\$PATH" >> ~/.bashrc
boop-photos$ source ~/.bashrc
```

## Usage

### Getting started

First, create a project:

```console
$ mkdir my-galleries && cd my-galleries
my-galleries$ mkdir theme photos
```

Create a two [Jinja](https://jinja.palletsprojects.com/) templates.

`theme/index.html.j2`:

```html
<!DOCTYPE html>
<html lang="en-GB">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1.0" />
        <title>My galleries</title>
    </head>
    <body>
        <h1>My galleries</h1>

        {% for gallery in galleries -%}
            {% if not gallery.private %}
                <div class="gallery">
                    <a href="{{ gallery.url }}">
                        <img src="{{ gallery.cover_photo.thumb_url }}" alt="" />
                        <span class="gallery__name">
                            {{ gallery.name }}
                        </span>
                    </a>
                </div>
            {% endif %}
        {% endfor %}
    </body>
</html>
```

`theme/gallery.html.j2`:

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1.0" />
        <title>{{ gallery.name }} — My galleries</title>
    </head>
    <body>
        <h1>{{ gallery.name }}</h1>
        <a href="index.html">Back to galleries</a>

        {% for photo in gallery.photos %}
            <div class="photo">
                <a href="{{ photo.url }}" target="_blank" rel="noreferrer">
                    <img src="{{ photo.thumb_url }}" alt="" />
                </a>
            </div>
        {% endfor %}
    </body>
</html>
```

Then, you can create your first gallery:

```console
my-galleries$ mkdir photos/my-gallery
my-galleries$ cp path/to/some/photos photos/my-gallery
```

And finally, generate your gallery under a `_site/` folder:

```console
my-galleries$ boop-photos.py
```

You can start a webserver with:

```console
my-galleries$ python3 -m http.server -d _site/ 8080
```

And open your website at [localhost:8080](http://localhost:8080). Nothing
fancy, but it's a good start!

**Remember: each time you make a change, you'll have to call the `boop-photos.py`
command!** Also, never directly change the `_site/*` files, they would be
overwritten.

In this quick tutorial, you learnt how to create a basic galleries website.
There are few other things to learn, the documentation will guide you.

### Declare gallery metadata

A gallery is a subfolder with photos under the `photos/` folder. You learnt how
to create one in the previous section.

You also can declare information about your gallery in a `metadata.yml` file.
This file must be placed in your gallery folder (e.g. `photos/my-gallery/metadata.yml`):

```yaml
---
name: "A name for your gallery"
cover: "cover-photo.jpg"
password: "a-secret-string"
```

Each key is optional.

If a `cover` is not declared, Boop! Photos will choose a photo (almost)
randomly by itself.

The `password` key allows to create secret galleries: they will not appear on
the index page and the password will be appended to the base <abbr>URL</abbr>
of the gallery (e.g. `https://example.com/my-gallery-a-secret-string.html`).
**Note that anyone who knows the <abbr>URL</abbr> will have access to your
gallery.**

### Manage static files

You'll probably want to write static files (e.g. CSS and JavaScript). Create a
folder named `theme/static/` and put a file in it:

```console
my-galleries$ mkdir theme/static/
my-galleries$ echo 'body { background-color: pink; }' > theme/static/style.css
```

You can then refer to this file in your templates:

```html
<link rel="stylesheet" href="static/style.css">
```

All the files under `theme/static/` are copied without being touched under
`_site/static/`.

### Configure Boop! Photos

You can change the default folders (`theme`, `photos` and `_site`) by creating
a `configuration.yml` file at the root of your project:

```yml
theme_dirname: "MyTheme"
photos_dirname: "galleries"
output_dirname: "output"
```

Also, all the variables that you define in this file will be available in your
templates via the `configuration` variable, for instance:

```html
<title>{{ configuration.website_title }}</title>
```

## Real-life example

[photos.marienfressinaud.fr](https://framagit.org/marienfressinaud/photos.marienfressinaud.fr)
